int getRelayLevel(char state) {
  switch (state) {
    case 'x':
      return relayOff;
    case 'c':
      return relayOn;
    default:
      return -1;
  }
}

void shutdownRelay(int relayIndex) {
  secondsTillOver[relayIndex] = 0;
  setRelay(relayIndex, relayOff);
}

void setActionableRelay(int relayIndex, int seconds) {
  relayMask[relayIndex] = relayOn;
  secondsTillOver[relayIndex] = seconds;
}

void actionRelays() {
  for (int i = 0; i < PIN_MASK_SIZE; i++) {
    if (relayMask[i] != -1) {
      digitalWrite(pinMask[i], relayMask[i]);
      delay(50);
      if (relayMask[i] == relayOn) {
        isRelayOn[i] = true;
        timeElapsed[i] = 0;
        printOnSerial("Relay ");
        printOnSerial(i);
        printlnOnSerial(" ON!");
        delay(50);
      }
    }
  }
  delay(1000);
}

void setRelays(int pinState) {
  printlnOnSerial("Set relays...");
  for (int i = 0; i < PIN_MASK_SIZE; i++) {
    relayMask[i] = pinState;
    digitalWrite(pinMask[i], relayMask[i]);
  }
  printlnOnSerial("Relays setted");
}

void setRelay(int relayIndex, int state) {
  relayMask[relayIndex] = state;
  digitalWrite(pinMask[relayIndex], relayMask[relayIndex]);
  printOnSerial("SETTING RELAY ");
  printOnSerial(relayIndex);
  printOnSerial(" TO ");
  printlnOnSerial(state);
}
void checkRelayJobDone() {
  for (int i = 0; i < PIN_MASK_SIZE; i++) {
    if (isRelayOn[i] && (int)(timeElapsed[i]/ONE_SECOND_IN_MS) > secondsTillOver[i]) {
      isRelayOn[i] = false;
      setRelay(i, relayOff);
//      printOnSerial("Relay ");
//      printOnSerial(i);
//      printlnOnSerial(" is OFF!");
      delay(50);
    }
  }
}

