#include <elapsedMillis.h>
#include <SoftwareSerial.h>
SoftwareSerial mySerial(2, 3);

#define PIN_MASK_SIZE 4
#define MAX_NUMBER_OF_MINUTES 5
#define ONE_MINUTE_IN_SECONDS 60
#define ONE_SECOND_IN_MS 1000
#define SERIAL_BAUD_RATE 9600
#define SERIAL_SIM_BAUD_RATE 56700

elapsedMillis timeElapsed[PIN_MASK_SIZE] = new elapsedMillis[PIN_MASK_SIZE];
String message;
bool isReadingMsg;
bool isRelayOn[PIN_MASK_SIZE];
int pinMask[] = {4, 5, 6, 7};
int secondsTillOver[PIN_MASK_SIZE];
int relayMask[25];
int relayOff = HIGH;
int relayOn = LOW;

char RELAY_ON_CHAR = 'c';
char RELAY_OFF_CHAR = 'x';
char RELAY_NEUTRAL_CHAR = 'o';

char MSG_END_CHAR = '#';
char MSG_START_CHAR = '$';
char MINUTE_CHAR = 'm';
char SECONDS_CHAR = 's';

//***************DEBUGGING FLAGS
bool debugging = true;
bool useModule = false;


void setupPins() {
  for (int i = 0; i < PIN_MASK_SIZE; i++) {
    pinMode(pinMask[i], OUTPUT);
  }
}

void setup() {
  setupPins();
  setRelays(relayOff);
  prepareSim();
  preparePortSerial();
}

void loop() {
  if (useModule) {
    readFromSimSerial();
  } else {
    readFromMockData();
    checkRelayJobDone();
    readFromAnotherMockData();
  }
}

