String minutesAsString;
int minutes[PIN_MASK_SIZE];
// $xc1m30soc2m#

void deserializeMessage(String message) {
  printOnSerial("DESERIALIZE MESSAGE: ");
  printlnOnSerial(message);

  int bellIndex = 0;
  for (int i = 0; i < message.length(); i++) {

    if (message[i] == RELAY_NEUTRAL_CHAR) {
      bellIndex++;
    }

    if (message[i] == RELAY_OFF_CHAR) {
      shutdownRelay(bellIndex++);
    }

    if (message[i] == RELAY_ON_CHAR) {
      int relayTime = stringTimeToIntSeconds(getTime(message, i + 1));
      setActionableRelay(bellIndex++, relayTime);
      printOnSerial("Setting relay ");
      printOnSerial(bellIndex - 1);
      printOnSerial(" to ON for ");
      printOnSerial(relayTime);
      printlnOnSerial(" seconds");

      delay(100);
    }
  }
  actionRelays();
}
String getTime(String message, int start) {
  String timeMessage = "";
  for (int j = start; j < message.length(); j++) {
    if (message[j] == 'c' || message[j] == 'o' || message[j] == 'x') {
      break;
    }
    timeMessage += message[j];
  }
  return timeMessage;
}

void parseMessage(char charRead) {
  if (isReadingMsg) {
    if (charRead == MSG_END_CHAR) { //this must be the last char from the message
      deserializeMessage(message);
      isReadingMsg = false;
      delay(1000);
      resetMessage();
    }
    message += charRead;
  }
  if (charRead == MSG_START_CHAR) {
    message = "";
    isReadingMsg = true;
  }
}

void resetMessage() {
  if (useModule) {
    printlnOnSerial("Waiting for message");
    message = "";
    isReadingMsg = false;

    mySerial.write("AT+CMGD=4\r");
    delay(1000);

    // wait for next message
    mySerial.write("AT+CNMI=1,2,0,0,0\r");
    mySerial.flush();
  }
}
