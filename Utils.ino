int minutesToSeconds(int minutes) {
  return minutes * ONE_MINUTE_IN_SECONDS;
}

int stringTimeToIntSeconds(String t) {
  int timeSeconds = 0;
  int secondsIndexStart;
  String minutesString = "";
  String secondsString = "";
  if (t.indexOf('m') != -1) {
    secondsIndexStart = t.indexOf('m') + 1;
    for (int i = 0; i < t.indexOf('m'); i++) {
      minutesString += t[i];
    }
  } else {
    secondsIndexStart = 0;
  }

  if (t.indexOf('s') != -1) {
    for (int j = secondsIndexStart; j < t.indexOf('s'); j++) {
      secondsString += t[j];
    }
  }
  timeSeconds = minutesToSeconds(minutesString.toInt()) + secondsString.toInt();
  return timeSeconds;
}

